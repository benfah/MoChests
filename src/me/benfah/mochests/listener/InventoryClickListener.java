package me.benfah.mochests.listener;

import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import me.benfah.mochests.block.ChestOversize;
import me.benfah.mochests.block.util.InventoryArray;

public class InventoryClickListener implements Listener
{
	
	@EventHandler
	public void onClick(InventoryClickEvent e)
	{
//		for(Entry<Location, Inventory> entr : ChestBase.hbm.entrySet())
//		{
//			if(e.getClickedInventory().getViewers().equals(entr.getValue().getViewers()))
//			{
//				if(e.getSlot() > e.getInventory().getSize() - 9)
//				{
//					e.setCancelled(true);
//				}
//			}
//		}
		if(e.getCurrentItem() != null)
		{	
		if(((e.isShiftClick() && e.getCurrentItem().getType() == Material.PAPER) || (e.getCurrentItem().getType() == Material.BARRIER)) && e.getInventory().getName().startsWith("Page"))
		e.setCancelled(true);
		
		
		if(e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.PAPER && (e.getSlot() == e.getInventory().getSize() - 1 || e.getSlot() == e.getInventory().getSize() - 9))
		for(Entry<Location, InventoryArray> entr : ChestOversize.hbm.entrySet())
		{
			int i = 0;
			for(Inventory inv : entr.getValue().invarray)
			{
				
				if(e.getInventory().getViewers().equals(inv.getViewers()))
				{
					if(e.getSlot() == e.getInventory().getSize() - 1)
						entr.getValue().openInventory((Player) e.getWhoClicked(), i+1);
					
					if(e.getSlot() == e.getInventory().getSize() - 9)
						entr.getValue().openInventory((Player) e.getWhoClicked(), i-1);
					
					break;
				}
				i++;
			}
		}
		}
	}
	
	
}
