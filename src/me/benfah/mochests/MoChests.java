package me.benfah.mochests;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

import me.benfah.cu.api.CustomRegistry;
import me.benfah.mochests.block.ChestBase;
import me.benfah.mochests.block.ChestOversize;
import me.benfah.mochests.block.TestItem;
import me.benfah.mochests.listener.InventoryClickListener;

public class MoChests extends JavaPlugin
{
	
	static ChestBase chestCopper;
	static ChestBase chestIron;
	static ChestOversize chestSilver;
	static ChestOversize chestGold;
	static ChestOversize chestDiamond;

	public static MoChests instance;
	
	@Override
	public void onEnable()
	{
		instance = this;
		
		chestCopper = new ChestBase("chest_copper", "block/chest_copper", "Copper Chest", 45)
		{
			@Override
			public Recipe getRecipe()
			{
				ShapedRecipe sr = new ShapedRecipe(getBlockItem());
				sr.shape("CIC", "I I", "CIC");
				sr.setIngredient('C', Material.COAL);
				sr.setIngredient('I', Material.IRON_INGOT);
				return sr;
			}
		};
		
		chestIron = new ChestBase("chest_iron", "block/chest_iron", "Iron Chest", 54)
		{
			@Override
			public Recipe getRecipe()
			{
				ShapedRecipe sr = new ShapedRecipe(getBlockItem());
				sr.shape("III", "I I", "III");
				sr.setIngredient('I', Material.IRON_INGOT);
				return sr;
			}
		};
		
		chestSilver = new ChestOversize("chest_silver", "block/chest_silver", "Silver Chest", 72)
		{
			@Override
			public Recipe getRecipe()
			{
				ShapedRecipe sr = new ShapedRecipe(getBlockItem());
				sr.shape("GIG", "I I", "GIG");
				sr.setIngredient('I', Material.IRON_INGOT);
				sr.setIngredient('G', Material.GOLD_INGOT);
				return sr;
			}
		};
		
		chestGold = new ChestOversize("chest_gold", "block/chest_gold", "Gold Chest", 81)
		{
			@Override
			public Recipe getRecipe()
			{
				ShapedRecipe sr = new ShapedRecipe(getBlockItem());
				sr.shape("GGG", "G G", "GGG");
				sr.setIngredient('G', Material.GOLD_INGOT);
				return sr;
			}
		};		
		chestDiamond = new ChestOversize("chest_diamond", "block/chest_diamond", "Diamond Chest", 108)
		{
			@Override
			public Recipe getRecipe()
			{
				ShapedRecipe sr = new ShapedRecipe(getBlockItem());
				sr.shape("DDD", "D D", "DDD");
				sr.setIngredient('D', Material.DIAMOND);
				return sr;
			}
		};	
		
		Bukkit.getPluginManager().registerEvents(new InventoryClickListener(), this);
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, new UpdateRunnable(), 6000L, 6000L);
		
		CustomRegistry.registerBlock(chestCopper, this);
		CustomRegistry.registerBlock(chestIron, this);
		CustomRegistry.registerBlock(chestSilver, this);
		CustomRegistry.registerBlock(chestGold, this);
		CustomRegistry.registerBlock(chestDiamond, this);
		CustomRegistry.registerItem(new TestItem(), this);
	}
	
	@Override
	public void onDisable()
	{
		ChestBase.closePlayerInventories();
		ChestOversize.closePlayerInventories();
		ChestBase.saveLocations();
		ChestOversize.saveLocations();
	}
	
	
	
}
