package me.benfah.mochests.block.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.benfah.mochests.MoChests;

public class InventoryArray
{
	
	public List<Inventory> invarray = new ArrayList<Inventory>();
	
	public InventoryArray(int size)
	{
		if(((((double)size) / 9D) % 1) == 1)
		throw new IllegalArgumentException("Argument size must be divisible by 9");
		
		final int maxSize = (int) Math.ceil(size / 45D);
		int i = 0;
		while(size > 45)
		{
			invarray.add(createInventory(45, i > 1 ? 1 : i, i + 1, maxSize));
			i++;
			size = size - 45;
		}
		if(size != 0)
		invarray.add(createInventory(size, 2, i + 1, maxSize));
		
	}
	
	public ItemStack createStack(Material type, String title)
	{
		ItemStack stack = new ItemStack(type);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(ChatColor.RESET + title);
		stack.setItemMeta(meta);
		return stack;
	}
	
//	public List<Map<String, Object>> serialize()
//	{
//		ArrayList<Map<String, Object>> list = new ArrayList<>();
//		for(Inventory inv : invarray)
//		{
//			int slot = 0;
//			for(ItemStack stack : inv.getContents())
//			{
//				if(!(inv.getSize() - 10 >= slot))
//				list.add(stack.serialize());
//				if(stack != null)
//				System.out.println(stack.getType());
//				
//				System.out.println(Material.AIR);
//
//				slot++;
//			}
//		}
//		return list;
//	}
	
	
	
	public void openInventory(Player p)
	{
		p.closeInventory();
		Bukkit.getScheduler().runTaskLater(MoChests.instance, new Runnable() {
			
			@Override
			public void run()
			{
				p.openInventory(invarray.get(0));
			}
		}, 1L);	}
	public void openInventory(Player p, int a)
	{
		p.closeInventory();
		Bukkit.getScheduler().runTaskLater(MoChests.instance, new Runnable() {
			
			@Override
			public void run()
			{
				p.openInventory(invarray.get(a));
			}
		}, 1L);
	}
	public Inventory createInventory(int size, int state, int index, int maxIndex)
	{
		size += 9;
		Inventory inv = Bukkit.createInventory(null, size, "Page " + index + "/" + maxIndex);
		
		for(int i = 0; i < 9; i++)
		inv.setItem(size - i - 1, createStack(Material.BARRIER, ""));
		
		if(state != 2)
		inv.setItem(size - 1, createStack(Material.PAPER, "Next Page"));
		
		if(state != 0)
		inv.setItem(size - 9, createStack(Material.PAPER, "Previous Page"));
		
		return inv;

	}

	
	
}
