package me.benfah.mochests.block;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.meta.ItemMeta;

import me.benfah.cu.api.BlockInstance;
import me.benfah.cu.api.CustomBlock;
import me.benfah.cu.api.IInstanceProvider;
import me.benfah.cu.util.ReflectionUtils;

public abstract class ChestBase extends CustomBlock implements IInstanceProvider
{
	int size;
	public ChestBase(String name, String modelPath, String title, int size)
	{
		super(name, modelPath, title);
		
		this.size = size;
	}
	
	public static ConcurrentHashMap<Location, Inventory> hbm = new ConcurrentHashMap<>();
	
	
	@Override
	public void onInteract(PlayerInteractEvent e)
	{
		BlockInstance bi = BlockInstance.getBlockInstance(e.getClickedBlock());
		openPlayerInventory(e.getPlayer(), bi.getLocation());
		if(e.getHand().equals(EquipmentSlot.HAND))
		{
			e.getClickedBlock().getWorld().playSound(e.getClickedBlock().getLocation(), Sound.BLOCK_CHEST_OPEN, 1, 1);
		}
		super.onInteract(e);
	}


	@Override
	public Class<?> getInstanceName()
	{
		return BlockInstance.class;
	}
	
	@Override
	public void onBlockBroken(BlockBreakEvent e)
	{
		removePlayers(e.getBlock().getLocation(), true);
		super.onBlockBroken(e);
	}
	
	public static void saveLocations()
	{
		
		for(Entry<Location, Inventory> entr : hbm.entrySet())
		{
			BlockInstance bi = BlockInstance.getBlockInstance(entr.getKey().getBlock());
			for(int i = 0; i < entr.getValue().getContents().length; i++)
			{
				ItemStack stack = entr.getValue().getContents()[i];
				bi.removeMetadataValue(i + "");
				if(stack != null)
				{
					Map<String, Object> map = stack.serialize();
					map.put("meta", ((ItemMeta)map.get("meta")).serialize());
					bi.setMetadataValue(i + "", map);
				}
			}
		}
	}
	
	@Override
	public ItemStack[] getLoot(Block b)
	{
		ItemStack[] stack = super.getLoot(b);
		if(hbm.containsKey(b.getLocation()))
		{
			stack = (ItemStack[]) ArrayUtils.addAll(super.getLoot(b), hbm.get(b.getLocation()).getContents());
		}
		return stack;
	}
	
	public static void removePlayers(Location loc, boolean remove)
	{
		for(Entry<Location, Inventory> entr : hbm.entrySet())
		{
			if(entr.getKey().equals(loc))
			{	
			for(HumanEntity p : entr.getValue().getViewers())
			{
				p.closeInventory();
			}
			if(remove)
			hbm.remove(entr.getKey());
			}
		}
		
	}
	
	public static void closePlayerInventories()
	{
		for(Entry<Location, Inventory> entr : hbm.entrySet())
		{
			
			for(HumanEntity p : entr.getValue().getViewers())
			{
				p.closeInventory();
			}
			
		}
	}
	
	public void openPlayerInventory(Player p, Location blockLoc)
	{
		if(hbm.containsKey(blockLoc))
		{	
		p.openInventory(hbm.get(blockLoc));
		return;
		}
		
		ItemStack[] arr = new ItemStack[size];
		BlockInstance bi = BlockInstance.getBlockInstance(blockLoc.getBlock());
		for(int i = 0; i < size; i++)
		{
			if(BlockInstance.getBlockInstance(blockLoc.getBlock()).hasMetadataValue(i + ""))
			{
				Map<String, Object> map =  (Map<String, Object>) bi.getMetadataValue(i + "");
				Class<?> clazz = ReflectionUtils.getRefClass("{cb}.inventory.CraftMetaItem$SerializableMeta");
				try {
					clazz.getMethods();
					if(!(map.get("meta") instanceof ItemMeta))
					map.put("meta", clazz.getMethod("deserialize", Map.class).invoke(null, map.get("meta")));
				} catch (ReflectiveOperationException e) {
					e.printStackTrace();
				} 
				arr[i] = ItemStack.deserialize(map);
			}
			
			
		}
		
		Inventory inv = Bukkit.createInventory(null, size);
		if(arr != null)
		inv.setContents(arr);
		p.openInventory(inv);
		hbm.put(blockLoc, inv);
	}
	
	
	@Override
	public abstract Recipe getRecipe();
	
}
