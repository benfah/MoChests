package me.benfah.mochests.block;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.benfah.cu.api.BlockInstance;
import me.benfah.mochests.block.util.InventoryArray;
import me.benfah.mochests.block.util.InventorySerialization;

public abstract class ChestOversize extends ChestBase
{

	public ChestOversize(String name, String modelPath, String title, int size)
	{
		super(name, modelPath, title, size);
	}
	
	public static ConcurrentHashMap<Location, InventoryArray> hbm = new ConcurrentHashMap<>();

	public static void saveLocations()
	{
		
		for(Entry<Location, InventoryArray> entr : hbm.entrySet())
		{
			BlockInstance bi = BlockInstance.getBlockInstance(entr.getKey().getBlock());
			int i = 0;
			for(Inventory inv : entr.getValue().invarray)
			{
				String ser = InventorySerialization.toBase64(inv);
				
				bi.setMetadataValue("" + i, ser);
				i++;
			}
			
		}
	}
	
	public static void closePlayerInventories()
	{
		for(Entry<Location, InventoryArray> entr : hbm.entrySet())
		{
			for(Inventory inv : entr.getValue().invarray)
			for(HumanEntity p : inv.getViewers())
			{
				p.closeInventory();
			}
			
		}
	}
	
	
	@Override
	public void onBlockBroken(BlockBreakEvent e)
	{
		removePlayers(e.getBlock().getLocation(), true);
		super.onBlockBroken(e);
	}
	
	public static void removePlayers(Location loc, boolean remove)
	{
		for(Entry<Location, InventoryArray> entr : hbm.entrySet())
		{
			if(entr.getKey().equals(loc))
			{	
			for(Inventory inv : entr.getValue().invarray)
			for(HumanEntity p : inv.getViewers())
			{
				p.closeInventory();
			}
			if(remove)
			hbm.remove(entr.getKey());
			}
		}
		
	}
	
	
	
	public void openPlayerInventory(Player p, Location blockLoc)
	{
//		if(CustomRegistry.isCustomBlock(blockLoc.getBlock()) && CustomRegistry.getCustomBlockByBlock(blockLoc.getBlock()) instanceof ChestBase)	
//		hbm.put(blockLoc, inv);
		if(hbm.containsKey(blockLoc))
		{	
		hbm.get(blockLoc).openInventory(p);
		return;
		}
		
		List<Inventory[]> larr = new ArrayList<>();
		
		BlockInstance bi = BlockInstance.getBlockInstance(blockLoc.getBlock());
		
		InventoryArray ia = new InventoryArray(size);
		int i = 0;
		while(bi.hasMetadataValue("" + i))
		{
			try
			{
				ia.invarray.set(i, InventorySerialization.fromBase64((String) bi.getMetadataValue("" + i)));
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			i++;
		}
		
		ia.openInventory(p);
		hbm.put(blockLoc, ia);
	}
	@Override
	public ItemStack[] getLoot(Block b)
	{
		ArrayList<ItemStack> result = new ArrayList<ItemStack>(Arrays.asList(super.getLoot(b)));
		
		if(hbm.containsKey(b.getLocation()))
		{
			List<ItemStack> stack = new ArrayList<>();
			InventoryArray arr = hbm.get(b.getLocation());
			for(Inventory inv : arr.invarray)
			{
				ItemStack[] sarr = inv.getContents();
//				ItemStack[] sarr = inv.getContents();
				for(int i = 0; i < inv.getContents().length; i++)
				{
					if(i < inv.getContents().length - 9)
					{
						result.add(sarr[i]);
					}
				}
			}
		}
		
		ItemStack[] stockArr = new ItemStack[result.size()];
		stockArr = result.toArray(stockArr);
		
		return stockArr;
	}
	
	
}
