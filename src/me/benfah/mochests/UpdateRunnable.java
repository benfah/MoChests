package me.benfah.mochests;

import me.benfah.mochests.block.ChestBase;
import me.benfah.mochests.block.ChestOversize;

public class UpdateRunnable implements Runnable
{

	@Override
	public void run()
	{
		ChestBase.saveLocations();
		ChestOversize.saveLocations();
	}

}
